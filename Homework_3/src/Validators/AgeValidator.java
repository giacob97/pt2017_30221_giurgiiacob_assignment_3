package Validators;
import model.User;

public class AgeValidator{
    
	private final int limit = 18;
	
	public void testAge(User u) throws Exception{
		if(u.getAge() < limit){
			throw new Exception("Minorii nu au drept de comanda");
		}
	}
}
