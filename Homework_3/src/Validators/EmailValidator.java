package Validators;

import model.User;




public class EmailValidator {
     private static final String c = ".com";
     
     public void test(User u) throws Exception{
    	 if(!u.getEmail().contains("@") || !(u.getEmail().contains(c)))
    		 throw new Exception("E-mail invalid!");
     }

}