package Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;





public class SQLConnection {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(SQLConnection.class.getName());
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/whouse?autoReconnect=true&useSSL=false";
	private static final String USER = "root";
	private static final String PASS = "root";
	
	public static SQLConnection singleInstance = new SQLConnection();
	Connection con ; 

	private SQLConnection(){
		try{
			Class.forName(DRIVER);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private Connection createConnection(){
		con = null;
		try {
			 con = (Connection) DriverManager.getConnection(DBURL,USER,PASS);
			//System.out.println("Ne-am conectat cu succes la baza de date "  + DBURL);
			return con ;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Connection getConnection(){
		 return singleInstance.createConnection();
	}
	
	public static void close(Connection connection){
		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("Nu s-a inchis corect conexiunea!");
		}
	}
	
	public static void close(PreparedStatement s){
		try {
			s.close();
		} catch (SQLException e) {
			System.out.println("Nu s-a inchis corect statement-ul!");
		}
	}
	
	
	public static void close(ResultSet rs){
		try {
			rs.close();
		} catch (SQLException e) {
			System.out.println("Nu s-a inchis corect resultSet-ul!");
		}
	}
	
}



