package factory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.SQLConnection;

import model.Stock;



public class StockFactory implements FactoryInterface<Stock> {
 
	
   private static final String insertStatementString = "INSERT INTO Stock (idStock,cantitate,idProdus) VALUES (?,?,?)";
   private static final String getLists = "SELECT * FROM Stock";
   private static String deleteStatementString = "delete from Stock where idStock = ";
	protected static final Logger LOGGER = Logger.getLogger(UserFactory.class.getName());  
	private static ArrayList<Stock> stocks = new ArrayList<>();

	
	public StockFactory(){
		stocks=generate();
	}
	
	@Override
	public ArrayList<Stock> generate() {
		ArrayList<Stock> aux = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try{
			connection = SQLConnection.getConnection();
			statement = connection.prepareStatement(getLists,Statement.RETURN_GENERATED_KEYS);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				int id = resultSet.getInt("idStock");
				int cantitate = resultSet.getInt("cantitate");
				int idp = resultSet.getInt("idProdus");
				aux.add(new Stock(id,cantitate,idp));
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
			SQLConnection.close(resultSet);
		}
		return aux;
	}
	
	
	@Override
	public int insert(Stock t) {
		Connection dbConnection = (Connection) SQLConnection.getConnection();
		PreparedStatement insertStatement = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, t.getId());
			insertStatement.setInt(2, t.getCantitate());
			insertStatement.setInt(3, t.getId_Produs());
			insertStatement.executeUpdate();
			stocks=generate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "User:insert " + e.getMessage());
		} finally {
			SQLConnection.close(insertStatement);
			SQLConnection.close(dbConnection);
		}
		return 1;
	}

	@Override
	public Stock find(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelection("idProdus");
		try{
		connection = SQLConnection.getConnection();
		statement =  connection.prepareStatement(query);
		statement.setInt(1, id);
		resultSet = statement.executeQuery();
		resultSet.next();
		int idS = resultSet.getInt("idStock");
		int cantitate = resultSet.getInt("cantitate");
		return new Stock(idS,cantitate,id);
		}catch(Exception e){System.out.println("Eroare de cautare");}
		finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
			SQLConnection.close(resultSet);
		}
		return null;
	}

	@Override
	public String createSelection(String column) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + "Stock WHERE " + column + "=?");
		return  sb.toString();
	}

	@Override
	public void update(Stock t, String cant) {
		Connection connection = SQLConnection.getConnection();;
	    Statement statement = null;
	    int toInt = Integer.parseInt(cant);
		try{
			statement =   connection.createStatement();
			statement.execute("update whouse.stock set cantitate='" + toInt + "'" +  " where idProdus=" + t.getId_Produs() + ";");
			statement.close();
			stocks=generate();
		}catch(Exception e){e.printStackTrace();}
		finally{
			SQLConnection.close(connection);
		}
		
	}
	@Override
	public void delete(Stock t) {
		deleteStatementString += t.getId();
		Connection connection = null;
		PreparedStatement statement = null;
		try{  
			connection = SQLConnection.getConnection();
			statement = connection.prepareStatement(deleteStatementString);
			statement.execute();
			stocks = generate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
		}
	}
		
	
	public static ArrayList<Stock> getStocks() {
		return stocks;
	}
	public static void setStocks(ArrayList<Stock> stocks) {
		StockFactory.stocks = stocks;
	}
}
