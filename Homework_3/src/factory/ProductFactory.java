package factory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.SQLConnection;
import model.Produs;
import model.Stock;



public class ProductFactory implements FactoryInterface<Produs>{
	private static final String insertStatementString = "INSERT INTO Produs (idProdus,nume,pret) VALUES (?,?,?)";
	private static String deleteStatementString = "delete from whouse.Produs where idProdus = ";
	protected static final Logger LOGGER = Logger.getLogger(UserFactory.class.getName());
	private static ArrayList<Produs> products = new ArrayList<>();
	private static final String getLists = "SELECT * FROM Produs";
	public List<Object> obiecte;
	public ProductFactory(){
		products = generate();
	}
	
	@Override
	public ArrayList<Produs> generate() {
		ArrayList<Produs> aux = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		obiecte  = new ArrayList<Object>();
		try{
			connection = SQLConnection.getConnection();
			statement = connection.prepareStatement(getLists,Statement.RETURN_GENERATED_KEYS);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				int id = resultSet.getInt("idProdus");
				String nume = resultSet.getString("nume");
				int p = resultSet.getInt("pret");
				obiecte.add(new Produs(id,nume,p));
				aux.add(new Produs(id,nume,p));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
			SQLConnection.close(resultSet);
		}
		return aux;
	}
	
	@Override
	public int insert(Produs t) {
		Connection dbConnection = (Connection) SQLConnection.getConnection();
		PreparedStatement insertStatement = null;
		StockFactory sf = new StockFactory();
		Random r = new Random();
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, t.getId());
			insertStatement.setString(2, t.getName());
			insertStatement.setInt(3, t.getPret());
			insertStatement.executeUpdate();
			sf.insert(new Stock(r.nextInt(50),r.nextInt(15)+7,t.getId()));
			generate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "User:insert " + e.getMessage());
		} finally {
			SQLConnection.close(insertStatement);
			SQLConnection.close(dbConnection);
		}
		return 1;
	}

	@Override
	public Produs find(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelection("idProdus");
		try{
		connection = SQLConnection.getConnection();
		statement =  connection.prepareStatement(query);
		statement.setInt(1, id);
		resultSet = statement.executeQuery();
		resultSet.next();
		String nume = resultSet.getString("nume");
		int p = resultSet.getInt("pret");
		return new Produs(id,nume,p);
		}catch(Exception e){System.out.println("Eroare de cautare");}
		finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
			SQLConnection.close(resultSet);
		}
		return null;
	}

	@Override
	public String createSelection(String column) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + "Produs WHERE " + column + "=?");
		return  sb.toString();
	}

	@Override
	public void update(Produs p,String text) {
		Connection connection = SQLConnection.getConnection();;
	    Statement statement = null;
		try{
			statement =   connection.createStatement();
			statement.execute("update whouse.produs set pret='" + text + "'" +  " where idProdus=" + p.getId() + ";");
			statement.close();
		    generate();
		}catch(Exception e){e.printStackTrace();}
		finally{
			SQLConnection.close(connection);
		}
	}
	@Override
	public void delete(Produs t) {
		StockFactory sf = new StockFactory();
		Stock s = sf.find(t.getId());
		sf.delete(s);
		deleteStatementString += t.getId();
		Connection connection = null;
		PreparedStatement statement = null;
		try{  
			connection = SQLConnection.getConnection();
			statement = connection.prepareStatement(deleteStatementString);
			statement.execute();
			generate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
		}
	}
	public static ArrayList<Produs> getProducts() {
		return products;
	}

	public static void setProducts(ArrayList<Produs> products) {
		ProductFactory.products = products;
	}

	

	
	    
}
