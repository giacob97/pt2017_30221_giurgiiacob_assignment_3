package factory;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import Connection.SQLConnection;
import Validators.EmailValidator;
import model.User;
//@SuppressWarnings("unused")

public class UserFactory implements FactoryInterface<User>{
	private static final String insertStatementString = "INSERT INTO User (idUser,nume,email,adresa,age) VALUES (?,?,?,?,?)";
	private static String deleteStatementString = "delete from User where idUser = ";
	private static final String getLists = "SELECT * FROM User";
	protected static final Logger LOGGER = Logger.getLogger(UserFactory.class.getName());
	private static ArrayList<User> users = new ArrayList<>();
	public List<Object> obiecte;
	public UserFactory() {
		 generate();
	}
    //public ArrayList<User> generate()
	public ArrayList<User> generate(){
		ArrayList<User> aux = new ArrayList<User>(0);
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		obiecte  = new ArrayList<Object>();
		try{
			connection = SQLConnection.getConnection();
			statement = connection.prepareStatement(getLists,Statement.RETURN_GENERATED_KEYS);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				int id = resultSet.getInt("idUser");
				String nume = resultSet.getString("nume");
				String email = resultSet.getString("email");
				String adresa = resultSet.getString("adresa");
				int age = resultSet.getInt("age");
				obiecte.add(new User(id,nume,email,adresa,age));
				aux.add(new User(id,nume,email,adresa,age));
                
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
			SQLConnection.close(resultSet);
		}
		return aux;
	}
	
	public int insert(User u){
		
		Connection dbConnection = (Connection) SQLConnection.getConnection();
		PreparedStatement insertStatement = null;
		try {
			EmailValidator ev = new EmailValidator();
			ev.test(u);
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, u.getId());
			insertStatement.setString(2, u.getName());
			insertStatement.setString(3, u.getEmail());
			insertStatement.setString(4, u.getAdress());
			insertStatement.setInt(5, u.getAge());
			insertStatement.executeUpdate();
			generate();
	
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "User:insert " + e.getMessage());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,e.getMessage());
		} finally {
			SQLConnection.close(insertStatement);
			SQLConnection.close(dbConnection);
		}
		return u.getId();
	}

	
	public User find(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelection("idUser");
		try{
		connection = SQLConnection.getConnection();
		statement =  connection.prepareStatement(query);
		statement.setInt(1, id);
		resultSet = statement.executeQuery();
		resultSet.next();
		String nume = resultSet.getString("nume");
		String adresa = resultSet.getString("adresa");
		String email = resultSet.getString("email");
		int age = resultSet.getInt("age");
		return new User(id,nume,email,adresa,age);
		}catch(Exception e){System.out.println("Eroare de cautare");}
		finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
			SQLConnection.close(resultSet);
		}
		return null;
	}

	public String createSelection(String column) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + "User WHERE " + column + "=?");
		return  sb.toString();
	}


	@Override
	public void update(User t, String newAddress) {
		
		Connection connection = SQLConnection.getConnection();
	    Statement statement = null;
		try{
			statement =   connection.createStatement();
			statement.execute("update whouse.user set adresa='" + newAddress + "'" +  " where idUser=" + t.getId() + ";");
			statement.close();
		    generate();
		}catch(Exception e){e.printStackTrace();}
		finally{
			SQLConnection.close(connection);
		}
	}

	public static ArrayList<User> getUsers() {
		return users;
	}

	public static void setUsers(ArrayList<User> users) {
		UserFactory.users = users;
	}

	@Override
	public void delete(User t) {
		deleteStatementString += t.getId();
		Connection connection = null;
		PreparedStatement statement = null;
		try{  
			connection = SQLConnection.getConnection();
			statement = connection.prepareStatement(deleteStatementString);
			statement.execute();
		 generate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
		}
	}
	
 
	
}
