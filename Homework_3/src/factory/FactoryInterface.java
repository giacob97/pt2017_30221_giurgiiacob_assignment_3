package factory;


import java.util.ArrayList;


public interface FactoryInterface<T> {
  
	public ArrayList<T> generate(); 
	public int insert(T t); // generam noul sir
	public T  find(int id);
	public String createSelection(String column);
    public void update(T t, String text); //generam noul sir
	public void delete(T t);  // generam noul sir
}
