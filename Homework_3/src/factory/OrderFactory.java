package factory;
import java.io.FileOutputStream;
import java.io.IOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import Connection.SQLConnection;
import Validators.AgeValidator;
import Validators.ProductValidator;
import model.Order;
import model.Stock;
import model.User;



public class OrderFactory implements FactoryInterface<Order>{

	private static final String insertStatementString = "INSERT INTO whouse.Order (idOrder,cantitate,id_produs,id_user,pret) VALUES (?,?,?,?,?)";
	protected static final Logger LOGGER = Logger.getLogger(UserFactory.class.getName());
	private static String deleteStatementString = "delete from whouse.Order where idOrder = ";
	private static ArrayList<Order> orders = new ArrayList<>();
	private static final String getLists = "SELECT * FROM whouse.Order";
	
	
	public OrderFactory(){
		orders = generate();
	}
	
	@Override
	public ArrayList<Order> generate() {
		ArrayList<Order> aux = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try{
			connection = SQLConnection.getConnection();
			statement = connection.prepareStatement(getLists,Statement.RETURN_GENERATED_KEYS);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				int id = resultSet.getInt("idOrder");
				int cantitate = resultSet.getInt("cantitate");
				int idp = resultSet.getInt("id_produs");
				int idu = resultSet.getInt("id_user");
				aux.add(new Order(id,cantitate,idp,idu));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
			SQLConnection.close(resultSet);
		}
		return aux;
	
	}

	
	@Override
	public int insert(Order t) {
		Connection dbConnection = (Connection) SQLConnection.getConnection();
		PreparedStatement insertStatement = null;
	    ProductFactory pf= new ProductFactory();
		StockFactory sf = new StockFactory();
		Stock s = sf.find(t.getIdProdus());
		UserFactory uf = new UserFactory();
		User u = uf.find(t.getIdUser());
		
		AgeValidator a = new AgeValidator();
		ProductValidator pv = new ProductValidator(); 
		try {
			a.testAge(u);
			pv.test(s, t.getCantitate());
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, t.getId());
			insertStatement.setInt(2, t.getCantitate());
			insertStatement.setInt(3, t.getIdProdus());
			insertStatement.setInt(4, t.getIdUser());
			t.setPret(t.getCantitate() * pf.find(t.getIdProdus()).getPret());
			System.out.println("Inainte : " + s.getCantitate());
			sf.update(s, String.valueOf(s.getCantitate() - t.getCantitate()));
			insertStatement.setInt(5, t.getPret());
			insertStatement.executeUpdate();
			orders = generate();
			
			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Comanda " + t.getId() +".pdf"));
	        document.open();
	        document.add(new Paragraph("Comanda numarul: " + t.getId()));
	        document.add(new Paragraph("Denumire produs: " + pf.find(t.getIdProdus()).getName()));
	        document.add(new Paragraph("Cantitate: "+ t.getCantitate()));
	        document.add(new Paragraph("Nume cumparator: " + uf.find(t.getIdUser()).getName()));
	        document.add(new Paragraph("Pret total: " + t.getPret() + " lei"));
	        document.close();
	        writer.close();
		     MailSender m = new MailSender();
		     //"marcipan_ddd@yahoo.com"
		     m.sendEmail("giurgi.iacob@gmail.com", "C:\\Users\\User\\Desktop\\TP\\HW_2\\Homework_3\\Comanda " + t.getId()+".pdf","Comanda " + t.getId()+".pdf");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Order:insert " + e.getMessage());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		} finally {
			SQLConnection.close(insertStatement);
			SQLConnection.close(dbConnection);
		}
		return 1;
	}

	@Override
	public Order find(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelection("idOrder");
		try{
		connection = SQLConnection.getConnection();
		statement =  connection.prepareStatement(query);
		statement.setInt(1, id);
		resultSet = statement.executeQuery();
		resultSet.next();
		int cantitate = resultSet.getInt("cantitate");
		int prod = resultSet.getInt("id_produs");
		int user = resultSet.getInt("id_user");
		return new Order(id,cantitate,prod,user);
		}catch(Exception e){System.out.println("Eroare de cautare");}
		finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
			SQLConnection.close(resultSet);
		}
		return null;
	}

	@Override
	public String createSelection(String column) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + "Order WHERE " + column + "=?");
		return  sb.toString();
	}

	@Override
	public void update(Order t, String text) {
		Connection connection = SQLConnection.getConnection();;
	    Statement statement = null;
		try{
			statement =   connection.createStatement();
			statement.execute("update whouse.order set cantitate='" + text + "'" +  " where idOrder=" + t.getId() + ";");
			statement.close();
			orders = generate();
		}catch(Exception e){e.printStackTrace();}
		finally{
			SQLConnection.close(connection);
		}
		
	}
    
	@Override
	public void delete(Order t) {
		deleteStatementString += t.getId();
		Connection connection = null;
		PreparedStatement statement = null;
		try{  
			connection = SQLConnection.getConnection();
			statement = connection.prepareStatement(deleteStatementString);
			statement.execute();
			orders = generate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			SQLConnection.close(connection);
			SQLConnection.close(statement);
		}
	}
	
	public static ArrayList<Order> getOrders() {
		return orders;
	}

	public static void setOrders(ArrayList<Order> orders) {
		OrderFactory.orders = orders;
	}
}
