package factory;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;

import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;



public class MailSender {

	public MailSender(){
		
	}
	   
	
	public void sendEmail(String to,String path,String fileName){
		try {
	        String host = "smtp.gmail.com";
	        String from = "javatest97@gmail.com";
	        String pass = "tehniciprogramare";
	        Properties props = System.getProperties();
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtp.user", from);
	        props.put("mail.smtp.password", pass);
	        props.put("mail.smtp.port", "587");
	        props.put("mail.smtp.auth", "true");
	       // props.put("mail.debug", "true");
	        
	        
	        Session session = Session.getInstance(props, new GMailAuthenticator("javatest97", pass));
	        MimeMessage message = new MimeMessage(session);
	        Address fromAddress = new InternetAddress(from);
	        Address toAddress = new InternetAddress(to);
	        
	        MimeBodyPart messageBodyPart = new MimeBodyPart();

	        Multipart multipart = new MimeMultipart();

	        messageBodyPart = new MimeBodyPart();
	        //String file = "C:\\Comanda 120966.pdf";
	        //DataSource source = new FileDataSource("C:\\Comanda 120966.pdf");
	        messageBodyPart.attachFile(path); //
	        messageBodyPart.setFileName(fileName);
	        multipart.addBodyPart(messageBodyPart);
	        
	        message.setFrom(fromAddress);
	        message.setRecipient(Message.RecipientType.TO, toAddress);
	        message.setSubject("Confirmare comanda");
	        message.setText("Ihaaa");
	        message.setContent(multipart);
	        Transport transport = session.getTransport("smtp");
	        transport.connect(host, from, pass);
	        message.saveChanges();
	        Transport.send(message);
	        transport.close();
	        System.out.println("E-mail sent!");
	    }catch(Exception ex){
           /*
	        System.out.println("<html><head></head><body>");
	        System.out.println("ERROR: " + ex);
	        System.out.println("</body></html>"); */
	    }
	}
	
}

class GMailAuthenticator extends Authenticator {
    String user;
    String pw;
    public GMailAuthenticator (String username, String password)
    {
       super();
       this.user = username;
       this.pw = password;
    }
   public PasswordAuthentication getPasswordAuthentication()
   {
      return new PasswordAuthentication(user, pw);
   }
}
