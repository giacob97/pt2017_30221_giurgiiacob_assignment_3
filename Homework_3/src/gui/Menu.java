package gui;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
@SuppressWarnings("unused")
public class Menu extends JFrame {
	
	private static final long serialVersionUID = 1L;
    JPanel contentPane;
	JScrollPane j ;
	JTextArea results,r;

	JButton user = new JButton("User\r\n");
	JButton product = new JButton("Product\r\n");
	JButton order = new JButton("Order\r\n");

	
	
	public Menu(){
		setTitle("Shop\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 400, 400);
		contentPane = new JPanel();
		contentPane.setBackground(Color.yellow);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		user.setBounds(145,80,100,50);
		product.setBounds(145,160,100,50);
		order.setBounds(145,240, 100,50);
		contentPane.add(user);
		contentPane.add(product);
		contentPane.add(order);
		user.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UserView u = new UserView();	
				dispose();
			}
			
		});
		product.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ProductView u = new ProductView();	
				dispose();
			}
			
		});
		order.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				OrderView u = new OrderView();
				dispose();
			}
			
		});
		setVisible(true);
	}
	
	
	
}
