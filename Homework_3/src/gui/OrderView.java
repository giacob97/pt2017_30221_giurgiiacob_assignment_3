package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


import factory.OrderFactory;
import model.Order;

public class OrderView extends JFrame{

	JButton add = new JButton("Add\r\n");
	private static final long serialVersionUID = 1L;
    JPanel contentPane;
    JButton back = new JButton("Back\r\n");
    
    public OrderView(){
    	Draw();
    }
    
	public void Draw(){
		setTitle("Order\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 700, 200);
		contentPane = new JPanel();
		contentPane.setBackground(Color.yellow);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		add.setBounds(280,100,100,50);
		contentPane.add(add);
		back.setBounds(600,135,70,20);
		contentPane.add(back);
		JLabel d = new JLabel("Cantitate\r\n"); 
		d.setBounds(100, 30, 200, 50);
		contentPane.add(d);

		JTextField textField1 = new JTextField(10);
		textField1.setBounds(175, 46, 70, 20);
		contentPane.add(textField1);
		
		JLabel d1 = new JLabel("Produs\r\n"); 
		d1.setBounds(290, 30, 200, 50);
		contentPane.add(d1);
		JTextField textField2 = new JTextField(10);
		textField2.setBounds(350, 46, 50, 20);
		contentPane.add(textField2);
		
		JLabel d3 = new JLabel("User\r\n"); 
		d3.setBounds(450, 30, 200, 50);
		contentPane.add(d3);
		JTextField textField3 = new JTextField(10);
		textField3.setBounds(490, 46, 50, 20);
		contentPane.add(textField3);
		
		add.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				try{
				Random r = new Random();
				String s = textField1.getText();
				int cant = Integer.parseInt(s);
				String str = textField2.getText();
				int idp = Integer.parseInt(str);
				String ss = textField3.getText();
				int idu = Integer.parseInt(ss);
				int a = r.nextInt(99999)+ 10000;
				OrderFactory of = new OrderFactory();	
				Order o = new Order((r.nextInt(99999)+ 10000),idu,idp,cant);
				of.insert(o);
				JOptionPane.showMessageDialog(null,"S-a facut comanda numarul " + a);
			}catch(Exception f){
				f.printStackTrace();
			}
			
			}});
		back.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				Menu m = new Menu();
				dispose();
			}});
		setVisible(true);
	}
	
}
