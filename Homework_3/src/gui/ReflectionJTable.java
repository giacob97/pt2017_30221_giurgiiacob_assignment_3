package gui;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;



public class ReflectionJTable {

	public static JTable createJTable(List<Object> list,int a){
		JTable t = new JTable();
		String columns[] = new String[a];
		int k = 0;
		Vector<Object>	data ;
		
		for(Field field : list.get(0).getClass().getDeclaredFields()){
		   columns[k] = field.getName();
		   k++;
		}
		DefaultTableModel m = new DefaultTableModel(columns,0);
		for(Object obj : list){
			data = new Vector<Object>();
			for(Field field : obj.getClass().getDeclaredFields()){
					try {
						field.setAccessible(true);
						data.addElement(field.get(obj));
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}catch(IllegalAccessException e){
						e.printStackTrace();
					}
		        }
			m.addRow(data);
		}
		t.setModel(m);
        return t;
	}
	
	
	
	
}
