package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import factory.ProductFactory;
import model.Produs;


public class ProductView extends JFrame{
	private static final long serialVersionUID = 1L;
    JPanel contentPane;
	JButton add = new JButton("Add\r\n");
	JButton edit = new JButton("Edit\r\n");
	JButton view = new JButton("View\r\n");
	JButton delete = new JButton("Delete\r\n");
	JButton back = new JButton("Back\r\n");
	public ProductView(){
		Draw();
		add.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				contentPane.removeAll();
				Draw();
				JLabel id = new JLabel("Id");
				JLabel nume = new JLabel("Nume");
				JLabel pret = new JLabel("Pret");
				JTextField f = new JTextField();
			    f.setBounds(400, 91, 150, 20);
			    JTextField f1 = new JTextField();
			    f1.setBounds(400, 91+50, 150, 20);
			    JTextField f2 = new JTextField();
			    f2.setBounds(400, 91+100, 150, 20);
			    JButton a = new JButton("Insert");
			    a.setBounds(400, 350, 100, 50);
				id.setBounds(300, 75, 100, 50);
				nume.setBounds(300, 125, 100, 50);
				pret.setBounds(300, 175, 100, 50);
				contentPane.add(id);
				contentPane.add(a);
				contentPane.add(nume);
				contentPane.add(pret);
				contentPane.add(f);
				contentPane.add(f1);
				contentPane.add(f2);
				a.addActionListener(new ActionListener(){
					
					@Override
					public void actionPerformed(ActionEvent e) {
						String idd = f.getText();
						int id = Integer.parseInt(idd);
						String nume = f1.getText();
						String pret_s = f2.getText();
						int pret = Integer.parseInt(pret_s);
						Produs p = new Produs(id,nume,pret);
						ProductFactory pf = new ProductFactory();
						pf.insert(p);
						JOptionPane.showMessageDialog(null,"S-a inserat produsul " + p.getName() + " cu pretul de " + p.getPret());
					}
				});
				contentPane.repaint();}});
	
	
	    view.addActionListener(new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent e){
			try{
			contentPane.removeAll();
			Draw();
			JTable table = new JTable();
			ProductFactory pf = new ProductFactory();
			pf.generate();
			System.out.println(pf.obiecte.size());
			table = ReflectionJTable.createJTable(pf.obiecte,3);
			JScrollPane j = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			j.setBounds(250, 46, 558, 369);
			contentPane.add(j);
			contentPane.repaint();
			}catch(Exception e1){
				e1.printStackTrace();
			}
			
		}});

	    edit.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				contentPane.removeAll();
				Draw();
				JLabel id = new JLabel("Id");
				JLabel pret = new JLabel("Pret");
				id.setBounds(300, 140, 100, 50);
				pret.setBounds(300, 190, 100, 50);
				JTextField f = new JTextField();
			    f.setBounds(400, 156, 150, 20);
			    JTextField f1 = new JTextField();
			    f1.setBounds(400,156+50, 150, 20);
			    contentPane.add(f);
				contentPane.add(f1);
				contentPane.add(id);
				contentPane.add(pret);
				JButton a = new JButton("Update");
			    a.setBounds(400, 300, 100, 50);
			    contentPane.add(a);
			    a.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						String idd = f.getText();
						int id1 = Integer.parseInt(idd);
						String pret = f1.getText();
						ProductFactory pf = new ProductFactory();
						Produs u = pf.find(id1);
						pf.update(u, pret);
						JOptionPane.showMessageDialog(null,"S-a modificat pretul produsului " + u.getName());
					}
			    });
				contentPane.repaint();
			}
	    });
	    back.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				Menu m = new Menu();
				dispose();
			}});
	    delete.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				contentPane.removeAll();
				Draw();
				JLabel id = new JLabel("Id");
				id.setBounds(300, 140, 100, 50);
				JTextField f = new JTextField();
			    f.setBounds(400, 156, 150, 20);
			    contentPane.add(f);
				contentPane.add(id);
				JButton a = new JButton("Delete");
			    a.setBounds(400, 250, 100, 50);
			    contentPane.add(a);
			    a.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						String idd = f.getText();
						int id1 = Integer.parseInt(idd);
						ProductFactory pf = new ProductFactory();
						Produs u = pf.find(id1);
						pf.delete(u);
						JOptionPane.showMessageDialog(null,"S-a sters produsul " + u.getName());
					}
			    });
			}
	    	
	    });
	}
	public void Draw(){
		setTitle("Product\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 1000, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.yellow);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		add.setBounds(50,80,100,50);
		edit.setBounds(50,160,100,50);
		view.setBounds(50,240, 100,50);
		delete.setBounds(50, 320, 100, 50);
		back.setBounds(900, 530, 70, 20);
		contentPane.add(back);
		contentPane.add(delete);
		contentPane.add(add);
		contentPane.add(edit);
		contentPane.add(view);
		setVisible(true);
	}
}
