package gui;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import factory.UserFactory;
import model.User;

public class UserView extends JFrame{
	private static final long serialVersionUID = 1L;
    JPanel contentPane;
	JButton add = new JButton("Add\r\n");
	JButton edit = new JButton("Edit\r\n");
	JButton view = new JButton("View\r\n");
	JButton delete = new JButton("Delete\r\n");
	JButton back = new JButton("Back\r\n");
	public UserView(){
		Draw();
		add.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				contentPane.removeAll();
				Draw();
				JLabel id = new JLabel("Id");
				JLabel nume = new JLabel("Nume");
				JLabel email = new JLabel("E-mail");
				JLabel adresa = new JLabel("Adresa");
				JLabel varsta = new JLabel("Varsta");
				JTextField f = new JTextField();
			    f.setBounds(400, 91, 150, 20);
			    JTextField f1 = new JTextField();
			    f1.setBounds(400, 91+50, 150, 20);
			    JTextField f2 = new JTextField();
			    f2.setBounds(400, 91+100, 150, 20);
			    JTextField f3 = new JTextField();
			    f3.setBounds(400, 91+150, 150, 20);
			    JTextField f4 = new JTextField();
			    JButton a = new JButton("Insert");
			    a.setBounds(400, 350, 100, 50);
			    f4.setBounds(400, 91+200, 150, 20);
				id.setBounds(300, 75, 100, 50);
				nume.setBounds(300, 125, 100, 50);
				email.setBounds(300, 175, 100, 50);
				adresa.setBounds(300, 225, 100, 50);
				varsta.setBounds(300, 275, 100, 50);
				contentPane.add(id);
				contentPane.add(a);
				contentPane.add(nume);
				contentPane.add(email);
				contentPane.add(adresa);
				contentPane.add(varsta);
				contentPane.add(f);
				contentPane.add(f1);
				contentPane.add(f2);
				contentPane.add(f3);
				contentPane.add(f4);
				a.addActionListener(new ActionListener(){
					
					@Override
					public void actionPerformed(ActionEvent e) {
						String idd = f.getText();
						int id = Integer.parseInt(idd);
						String nume = f1.getText();
						String email = f2.getText();
						String adresa = f3.getText();
						String age_s = f4.getText();
						int age = Integer.parseInt(age_s);
						User u = new User(id,nume,adresa,email,age);
						UserFactory uf = new UserFactory();
						uf.insert(u);
					    JOptionPane.showMessageDialog(null,"S-a inserat userul " + u.getName());
					}
				});
				contentPane.repaint();}});
	
	
	    view.addActionListener(new ActionListener(){
	
		@Override
		public void actionPerformed(ActionEvent e){
			try{
			contentPane.removeAll();
			Draw();
			JTable table = new JTable();
			UserFactory uf = new UserFactory();
			uf.generate();
			System.out.println(uf.obiecte.size());
			table = ReflectionJTable.createJTable(uf.obiecte,5);
			JScrollPane j = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			j.setBounds(250, 46, 558, 369);
			contentPane.add(j);
			contentPane.repaint();
			}catch(Exception e1){
				e1.printStackTrace();
			}
			
		}});

	    edit.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				contentPane.removeAll();
				Draw();
				JLabel id = new JLabel("Id");
				JLabel adresa = new JLabel("Adresa noua");
				id.setBounds(300, 140, 100, 50);
				adresa.setBounds(300, 190, 100, 50);
				JTextField f = new JTextField();
			    f.setBounds(400, 156, 150, 20);
			    JTextField f1 = new JTextField();
			    f1.setBounds(400,156+50, 150, 20);
			    contentPane.add(f);
				contentPane.add(f1);
				contentPane.add(id);
				contentPane.add(adresa);
				JButton a = new JButton("Update");
			    a.setBounds(400, 300, 100, 50);
			    contentPane.add(a);
			    a.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						String idd = f.getText();
						int id1 = Integer.parseInt(idd);
						String adress = f1.getText();
						UserFactory uf = new UserFactory();
						User u = uf.find(id1);
						uf.update(u, adress);
						JOptionPane.showMessageDialog(null,"S-a modificat adresa userul-ului " + u.getName());
					}
			    });
				contentPane.repaint();
			}
	    });
	    back.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				Menu m = new Menu();
				dispose();
			}});
	    delete.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				contentPane.removeAll();
				Draw();
				JLabel id = new JLabel("Id");
				id.setBounds(300, 140, 100, 50);
				JTextField f = new JTextField();
			    f.setBounds(400, 156, 150, 20);
			    contentPane.add(f);
				contentPane.add(id);
				JButton a = new JButton("Delete");
			    a.setBounds(400, 250, 100, 50);
			    contentPane.add(a);
			    a.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						String idd = f.getText();
						int id1 = Integer.parseInt(idd);
						UserFactory uf = new UserFactory();
						User u = uf.find(id1);
						uf.delete(u);
						JOptionPane.showMessageDialog(null,"S-a sters userul " + u.getName());
					}
			    });
			}
	    	
	    });
	}
	public void Draw(){
		setTitle("User\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 1000, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.yellow);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		add.setBounds(50,80,100,50);
		edit.setBounds(50,160,100,50);
		view.setBounds(50,240, 100,50);
		delete.setBounds(50, 320, 100, 50);
		back.setBounds(900, 530, 70, 20);
		contentPane.add(back);
		contentPane.add(delete);
		contentPane.add(add);
		contentPane.add(edit);
		contentPane.add(view);
		setVisible(true);
	}
}
