package model;

public class Order {
     private final int id;
     private int idUser;
     private int idProdus;
     private int cantitate;
	 private int pret;
     public Order(int id, int idUser, int idProdus, int cantitate) {
		super();
		this.id = id;
		this.idUser = idUser;
		this.idProdus = idProdus;
		this.cantitate = cantitate;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public int getIdProdus() {
		return idProdus;
	}

	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public int getId() {
		return id;
	}

	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}
      
    
     
}
