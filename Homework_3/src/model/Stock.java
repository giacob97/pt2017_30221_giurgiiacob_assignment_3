package model;

public class Stock {
      private final int id;
      private int cantitate;
      private int idProdus;
      
      
      
 
	public Stock(int id, int cantitate, int id_Produs) {
		super();
		this.id = id;
		this.cantitate = cantitate;
		this.idProdus = id_Produs;
	}
	public int getId() {
		return id;
	}

	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	public int getId_Produs() {
		return idProdus;
	}
	public void setId_Produs(int id_Produs) {
		this.idProdus = id_Produs;
	}
      
      
}
