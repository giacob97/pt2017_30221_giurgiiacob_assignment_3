package model;

public class Produs {
      
	 private final int id;
	 private String name;
	 private int pret;
	 
	 public Produs(int id, String name,int pret) {
		super();
		this.id = id;
		this.name = name;
		this.pret = pret;
	 }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}
	 
	
	 
	   
}
