-- MySQL Workbench Forward Engineering



CREATE DATABASE IF NOT EXISTS depozit;
USE depozit;
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `mydb`.`Client`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS Client (
 `nume` VARCHAR(45) NOT NULL,
  `telefon` VARCHAR(45) NOT NULL,
  `adresa` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`nume`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Produs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Produs (
  `nume` VARCHAR(45) NOT NULL,
  `idProdus` INT NOT NULL,
  `pret` FLOAT,
  PRIMARY KEY (`nume`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Courier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Courier (
  `idCourier` INT NOT NULL AUTO_INCREMENT, 
  `nume` VARCHAR(45) NULL,
  `masina` VARCHAR(45) NULL,
  PRIMARY KEY (`idCourier`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Order`
-- -----------------------------------------------------
 DROP TABLE Comanda
CREATE TABLE IF NOT EXISTS Comanda (
   `idOrder` INT(11) NOT NULL AUTO_INCREMENT,
    client_nume VARCHAR(45) ,
    `Produs_nume` VARCHAR(45) ,
    `Courier_idCourier` INT(11) ,
  `cantitate` INT NULL,
  PRIMARY KEY (`idOrder`),
  FOREIGN KEY(client_nume) references Client(`nume`),
  FOREIGN KEY(produs_nume) references Produs(`nume`),
  FOREIGN KEY(courier_idCourier) references Courier(`idCourier`)
  )
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS Stock(
  `cantitate` INT NOT NULL,
  `produs_nume` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`cantitate`, `produs_nume`),
  INDEX `fk_Stock_produs1_idx` (`produs_nume` ASC),
  CONSTRAINT `fk_Stock_produs1`
    FOREIGN KEY (`produs_nume`)
    REFERENCES `depozit`.`produs` (`nume`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

